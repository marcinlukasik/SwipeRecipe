module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json"),

        wiredep: {

            app: {

                // Point to the files that should be updated when
                // you run `grunt wiredep`
                src: [
                    'public/index.html'
                ],

                options: {
                    // See wiredep's configuration documentation for the options
                    // you may pass:

                    // https://github.com/taptapship/wiredep#configuration
                }
            }
        },

        sass: {
            dev: {
                options: {
                    style: "expanded"
                },
                src: "public/sass/app.scss",
                dest: "public/css/app.css"
            }
        },

        watch: {
            options: {
                livereload: false
            },
            dev: {
                files: ["public/sass/*", "public/css/*"],
                tasks: ["dev"]
            }
        }

    });

    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerTask("dev", ["wiredep", "sass"]);
    grunt.registerTask("default", "dev");

};