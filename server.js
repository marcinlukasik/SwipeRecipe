var express = require('express'),
    bodyParser = require('body-parser'),
    async = require('async'),
    mongo = require('mongodb'),
    MongoClient = mongo.MongoClient,
    bson = require('bson'),
    app = express(),
    dbUrl = 'mongodb://localhost:27017/swipe_recipe';

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.sendfile('index.html');
});

app.get('/recipes', function (req, res) {

    var limit = req.query.limit,
        skip = req.query.skip,
        order = parseInt(req.query.order),
        name = req.query.name,
        regex = new RegExp(name, 'ig');

    MongoClient.connect(dbUrl, function(err, db) {

        if(err) {
            res.status(500);
            res.json({ error: true });

            return;
        }

        db.collection('recipes').find({}/*{title: regex}, {limit: limit, skip: skip, sort: {title: order || 1}}*/).toArray(function(err, docs) {

            if(err) {
                res.status(500);
                res.json({ error: true });

                return;
            }

            res.json(docs);

            db.close();
        });
    });
});

app.get('/recipe/:id', function (req, res) {

    var id = req.params.id,
        isValid = bson.BSONPure.ObjectID.isValid(id);

    if(!isValid) {
        res.status(500);
        res.json({error: true});

        return;
    }

    MongoClient.connect(dbUrl, function(err, db) {

        if(err) {
            res.status(500);
            res.json({ error: true });

            return;
        }

        db.collection('recipes').find({_id: new mongo.ObjectID(id)}).toArray(function(err, docs) {

            if(err) {
                res.status(500);
                res.json({ error: true });

                return;
            }

            res.json(docs[0]);

            db.close();
        });
    });
});

app.post('/recipes', function (req, res) {

    MongoClient.connect(dbUrl, function(err, db) {

        if(err) {
            res.status(500);
            res.json({ error: true });

            return;
        }

        db.collection('recipes').insert(req.body, function(err, doc) {

            if(err) {
                res.status(500);
                res.json({ error: true });

                return;
            }

            res.json(doc[0]);

            db.close();
        });

    });
});

app.put('/recipe/:id', function (req, res) {

    var id = req.params.id,
        isValid = bson.BSONPure.ObjectID.isValid(id);

    if(!isValid) {
        res.status(500);
        res.json({error: true});

        return;
    }

    MongoClient.connect(dbUrl, function(err, db) {

        if(err) {
            res.status(500);
            res.json({ error: true });

            return;
        }

        delete req.body._id;

        db.collection('recipes').findAndModify({_id: new mongo.ObjectID(id)}, {}, {$set: req.body}, {new: true}, function(err, doc) {

            if(err) {
                res.status(500);
                res.json({ error: true });

                return;
            }

            res.json(doc);

            db.close();
        });
    });
});

app.get('/favourite/:id', function (req, res) {

    var id = req.params.id,
        favourite = req.query.favourite,
        isValid = bson.BSONPure.ObjectID.isValid(id);

    if(!isValid) {
        res.status(500);
        res.json({error: true});

        return;
    }

    MongoClient.connect(dbUrl, function(err, db) {

        if(err) {
            res.status(500);
            res.json({ error: true });

            return;
        }

        db.collection('recipes').findAndModify({_id: new mongo.ObjectID(id)}, {}, {$set: {favourite: favourite }}, {new: true}, function(err, doc) {

            if(err) {
                res.status(500);
                res.json({ error: true });

                return;
            }

            res.json(doc);

            db.close();
        });
    });
});

app.delete('/recipe/:id', function (req, res) {

    var id = req.params.id,
        isValid = bson.BSONPure.ObjectID.isValid(id);

    if(!isValid) {
        res.status(500);
        res.json({error: true});

        return;
    }

    MongoClient.connect(dbUrl, function(err, db) {

        if(err) {
            res.status(500);
            res.json({ error: true });

            return;
        }

        db.collection('recipes').findAndRemove({_id: new mongo.ObjectID(id)}, function(err, doc) {

            if(err) {
                res.status(500);
                res.json({ error: true });

                return;
            }

            res.json({deleted: true});

            db.close();
        });
    });
});

app.listen('8000', function() {
    console.log('Serwer aktywny!');
});

