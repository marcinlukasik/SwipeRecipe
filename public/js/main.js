requirejs.config({

    paths: {
        'jquery': 'libs/jquery/dist/jquery',
        'underscore': 'libs/underscore/underscore',
        'backbone': 'libs/backbone/backbone',
        'customsync': 'libs/backbone.customsync/backbone.customsync',
        'stickit': 'libs/backbone.stickit/backbone.stickit',
        'alertify': 'libs/alertify-js/build/alertify',
        'moment': 'libs/moment/min/moment.min',
        'materialize': 'libs/materialize/bin/materialize',
        'scrollFire': 'libs/materialize/js/scrollFire',
        'hammer' : 'libs/materialize/js/hammer.min',
        'velocity' : 'libs/materialize/js/velocity.min',
        'pushState': 'libs/pushstate-links',
        'text': 'libs/requirejs-text/text',
        'templates': '../templates'
    },

    shim: {
        'underscore': {
            exports: '_'
        },

        'backbone': {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },

        'materialize': {
            deps: [
                'jquery', 'hammer', 'velocity', 'scrollFire'
            ]
        }
    }
});

require([
    'app',
], function (App) {

    'use strict';

    App.initialize();
});
