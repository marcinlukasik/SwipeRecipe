define([
    'jquery',
    'underscore',
    'backbone',
    'collections/RecipesListCollection',
    'models/RecipeModel',
    'views/RecipesListView',
    'views/RecipeNewView',
    'views/RecipeDetailsView',
    'views/RecipeEditView'
], function ($, _, Backbone, RecipesListCollection, RecipeModel, RecipesListView, RecipeNewView, RecipeDetailsView, RecipeEditView) {

    'use strict';

    var AppRouter = Backbone.Router.extend({

        routes: {
            '': 'showRecipesList',
            'recipes': 'showRecipesList',
            'recipe/new': 'recipeNew',
            'recipe/:id': 'showRecipeDetails',
            'recipe/edit/:id': 'recipeEdit'
        },

        showRecipesList: function () {

            var recipes = new RecipesListCollection(),
                view = new RecipesListView({
                    collection: recipes
                    /*page: page,
                    order: order,
                    search: search*/
                });

            recipes.fetch({
                reset: true
                /*data: {
                    limit: 5,
                    skip: skip,
                    order: order,
                    name: search
                }*/
            });
        },

        recipeNew: function () {

            var recipe = new RecipeModel(),
                view = new RecipeNewView({model: recipe});
        },

        showRecipeDetails: function (id) {

            var recipe = new RecipeModel({_id: id}),
                view = new RecipeDetailsView({model: recipe});

            recipe.fetch();
        },

        recipeEdit: function (id) {

            var recipe = new RecipeModel({_id: id}),
                view = new RecipeEditView({model: recipe});

            recipe.fetch();
        }

    });

    var initialize = function() {

        var router = new AppRouter();

        Backbone.history.start({pushState: true});
    };

    return {
        initialize: initialize
    };
});