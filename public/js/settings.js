define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {

    var Settings = {

        Regions: {
            appMain: $('#app-main')
        }

    };

    return Settings;

});