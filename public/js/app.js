define([
    'jquery',
    'underscore',
    'backbone',
    'routers/router'
], function ($, _, Backbone, Router) {

    'use strict';

    var initialize = function() {

        var router = Router.initialize();
    };

    return {
        initialize: initialize
    };

});