define([
    'jquery',
    'underscore',
    'backbone',
    'models/RecipeModel'
], function($, _, Backbone, RecipeModel) {

    var RecipesListCollection = Backbone.Collection.extend({

        model: RecipeModel,

        url: '/recipes'
    });

    return RecipesListCollection;
});