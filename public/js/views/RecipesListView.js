define([
    'jquery',
    'underscore',
    'backbone',
    'settings',
    'views/RecipeListItemView',
    'views/ToolbarView',
    'text!templates/RecipesList.html',
    'text!templates/pagination.html'
], function($, _, Backbone, Settings, RecipeListItemView, ToolbarView, RecipesListTemplate, PaginationTemplate) {

    var RecipesListView = Backbone.View.extend({

        tagName: 'ul',
        className: 'collection',

        template: _.template( RecipesListTemplate ),

        initialize: function() {

            this.listenTo(this.collection, 'reset', this.render);
        },

        render: function() {

            this.collection.each(this.addOne, this);

            var region = Settings.Regions.appMain,
                toolbar = new ToolbarView();

            region.html( this.template );
            region.find('.app-collection > .col').append(this.el);
            region.prepend( toolbar.render().el );
            region.append( PaginationTemplate );
        },

        addOne: function(model) {

            var view = new RecipeListItemView({model: model});

            this.$el.append(view.render().el);
        }

    });

    return RecipesListView;
});