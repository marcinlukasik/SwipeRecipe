define([
    'jquery',
    'underscore',
    'backbone',
    'settings',
    'text!templates/RecipeEdit.html',
    'alertify',
    'stickit'
], function($, _, Backbone, Settings, RecipeEditTemplate, Alertify) {

    var RecipeEditView = Backbone.View.extend({

        tagName: 'div',
        className: 'row',

        template: _.template( RecipeEditTemplate ),

        initialize: function () {

            this.listenToOnce(this.model, 'change', this.render);
            this.listenToOnce(this.model, 'update', this.showUpdateInfo);
            this.listenToOnce(this.model, 'update', this.redirectToDetails);
            this.listenTo(this.model, 'invalid', this.showErrorInfo);

            console.log(this.model);
        },

        render: function () {

            var html = this.template( this.model.toJSON()),
                region = Settings.Regions.appMain;

            this.$el.html(html);
            region.html( this.$el );

            this.stickit();

            Alertify.set('notifier','position', 'top-right');

            return this;
        },

        bindings: {
            '#title': 'title',
            '#ingredients': {
                observe: 'ingredients',
                updateMethod: 'html',
                onSet: function(val) {

                    var newArr = [];

                    _.map(val.split('\n'),
                        function (n) {
                            newArr.push( '<li>' + n + '</li>' );
                        }
                    );

                    return newArr.join('');
                },
                onGet: function(val) {

                    return val.replace(/<li>/g, '').replace(/<\/li>/g, '\n').slice(0, -1);
                }
            },
            '#description': 'description'
        },

        events: {
            'click .btn-back': 'redirectBack',
            'click .btn-save': 'updateRecipe'
        },

        updateRecipe: function (e) {

            e.preventDefault();

            var model = this.model;

            this.model.save({}, {
                wait: true,
                success: function () {
                    model.trigger('update');
                }
            });
        },

        showUpdateInfo: function () {

            Alertify.notify('Update success!', 'success', 3);
        },

        showErrorInfo: function (model) {

            Alertify.notify(model.validationError, 'error', 3);
        },

        redirectToDetails: function () {

            Backbone.history.navigate('/recipe/' + this.model.get('_id'), {trigger: true});
        },

        redirectBack: function () {

            Backbone.history.navigate('/', {trigger: true});
        }

    });

    return RecipeEditView;

});