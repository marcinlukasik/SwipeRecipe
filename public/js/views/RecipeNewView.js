define([
    'jquery',
    'underscore',
    'backbone',
    'settings',
    'text!templates/RecipeEdit.html',
    'alertify',
    'moment',
    'stickit'
], function($, _, Backbone, Settings, RecipeEditTemplate, Alertify, Moment) {

    var RecipeNewView = Backbone.View.extend({

        tagName: 'div',
        className: 'row',

        template: _.template( RecipeEditTemplate ),

        initialize: function () {

            this.listenTo(this.model, 'sync', this.redirectToDetails);
            this.listenToOnce(this.model, 'sync', this.showAddedInfo);
            this.listenTo(this.model, 'invalid', this.showErrorInfo);

            this.render();
        },

        render: function () {

            var html = this.template( this.model.toJSON()),
                region = Settings.Regions.appMain;

            this.$el.html(html);
            region.html( this.$el );

            this.stickit();

            Alertify.set('notifier','position', 'top-right');

        },

        bindings: {
            '#title': 'title',
            '#ingredients': {
                observe: 'ingredients',
                updateMethod: 'html',
                onSet: function(val) {

                    var newArr = [];

                    _.map(val.split('\n'),
                        function (n) {
                            newArr.push( '<li>' + n + '</li>' );
                        }
                    );

                    return newArr.join('');
                }
            },
            '#description': 'description',
            '#date': {
                observe: 'date',
                /*onGet: function (val) {
                    return Moment().format('DD-MM-YYYY');
                },*/
                onSet: function (val) {
                    //return Moment().format('DD-MM-YYYY');

                    return '04-04-2015';
                }
            }
        },

        events: {
            'click .btn-back': 'redirectBack',
            'submit form': 'saveRecipe'
        },

        saveRecipe: function (e) {

            e.preventDefault();

            this.model.save({}, {
                wait: true,
                dataType: 'text',
                success: function (m, res, opt) {

                    console.log(res);
                },
                error: function (err) {

                    console.log('error');
                    console.log(err);
                }

            });

        },

        showAddedInfo: function () {

            Alertify.notify('Create success!', 'success', 3);
            console.log(this.model);
        },

        showErrorInfo: function (model) {

            Alertify.notify(model.validationError, 'error', 3);
        },

        redirectToDetails: function () {

            Backbone.history.navigate('/recipe/' + this.model.get('_id'), {trigger: true});
        },

        redirectBack: function () {

            Backbone.history.navigate('/', {trigger: true});
        }

    });

    return RecipeNewView;

});