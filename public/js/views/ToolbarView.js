define([
    'jquery',
    'underscore',
    'backbone',
    'settings',
    'text!templates/toolbar.html',
], function($, _, Backbone, Settings, ToolbarTemplate) {

    var ToolbarView = Backbone.View.extend({

        tagName: 'div',
        className: 'row',

        template: _.template( ToolbarTemplate ),

        render: function () {

            this.$el.html( this.template );

            return this;
        },

        events: {
            'click .btn-add': 'redirectToAdd'
        },

        redirectToAdd: function () {
            Backbone.history.navigate('/recipe/new', {trigger: true});
        }

    });

    return ToolbarView;

});