define([
    'jquery',
    'underscore',
    'backbone',
    'settings',
    'models/RecipeModel',
    'text!templates/RecipeListItem.html',
    'alertify',
], function($, _, Backbone, Settings, RecipeModel, RecipeListItemTemplate, Alertify) {

    var RecipesListItemView = Backbone.View.extend({

        tagName: 'li',
        className: 'collection-item avatar',

        template: _.template( RecipeListItemTemplate ),

        initialize: function () {

            this.listenToOnce(this.model, 'destroy', this.showRemoveInfo);
            this.listenTo(this.model, 'destroy', this.remove);
        },

        render: function() {

            var html = this.template( this.model.toJSON() );

            this.$el.html(html);

            return this;
        },

        events: {
            'click .like': 'updateFavourite',
            'click .btn-details': 'redirectToDetails',
            'click .btn-edit': 'redirectToEdit',
            'click .btn-remove': 'deleteRecipe'
        },

        updateFavourite: function(e) {

            if(!$(e.target).hasClass('green')) {
                $(e.target).addClass('green');

                var val = true;
            }
            else {
                $(e.target).removeClass('green');

                var val = false;
            }

            $.ajax({
                url: '/favourite/' + this.model.get('_id'),
                context: this,
                type: 'GET',
                data: {
                    favourite: val
                }
            });
        },

        deleteRecipe: function () {

            var model = this.model;

            model.destroy({wait: true});
        },

        showRemoveInfo: function () {

            Alertify.notify('Removed recipe.', 'success', 3);
        },

        redirectToDetails: function () {

            Backbone.history.navigate('/recipe/' + this.model.get('_id'), {trigger: true});
        },

        redirectToEdit: function () {

            Backbone.history.navigate('/recipe/edit/' + this.model.get('_id'), {trigger: true});
        }

    });

    return RecipesListItemView;
});