define([
    'jquery',
    'underscore',
    'backbone',
    'settings',
    'text!templates/RecipeDetails.html',
], function($, _, Backbone, Settings, RecipeDetailsTemplate) {

    var RecipeDetailsView = Backbone.View.extend({

        tagName: 'div',
        className: 'row',

        template: _.template( RecipeDetailsTemplate ),

        initialize: function() {

            this.listenTo(this.model, 'change', this.render);
        },

        render: function() {

            var html = this.template( this.model.toJSON()),
                region = Settings.Regions.appMain;

            this.$el.html(html);
            region.html( this.$el );

            return this;
        },

        events: {
          'click .btn-back': 'redirectToRecipes',
          'click .btn-edit': 'redirectToEdit'
        },

        redirectToRecipes: function () {

            Backbone.history.navigate('/', {trigger: true});
        },

        redirectToEdit: function () {

            Backbone.history.navigate('/recipe/edit/' + this.model.get('_id'), {trigger: true});
        }

    });

    return RecipeDetailsView;
});