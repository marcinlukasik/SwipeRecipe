define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {

    var RecipeModel = Backbone.Model.extend({

        idAttribute: '_id',

        defaults: {
            title: '',
            date: '',
            ingredients: '',
            description: '',
            favourite: false
        },

        url: function () {

            if(this.isNew()) {
                return '/recipes';
            }
            else {
                return '/recipe/' + this.get('_id');
            }
        },

        validate: function(attrs, options) {

            if(attrs.title === '') {
                return 'Field "Title" is required!';
            }

            if(attrs.ingredients === '') {
                return 'Field "Ingredients" is required!';
            }

            if(attrs.description === '') {
                return 'Field "Description" is required!';
            }

        }

    });

    return RecipeModel;
});